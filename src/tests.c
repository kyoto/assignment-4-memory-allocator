//
// Created by kyoto on 07.11.22.
//

#include "tests.h"


static void end_test( void *heap, int query, const char *const testName) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = query}).bytes);
    sucs(testName, " is successful!");
}

static void check_heap(const void *const heap, const char *const msg) {
    printf("%s: %s", msg, "\n");
    debug_heap(stdout, heap);
}

//Обычное успешное выделение памяти.
void test1(void) {
    void *heap = heap_init(4096);
        if (!heap) error("test1 failed", ", heap is not initialized." );
        check_heap(heap, "Heap after init");

    void *mem = _malloc(2048);
        if (!mem) error("test1 failed", ", memory is not allocated.");
        check_heap(heap, "Heap after alloc");

    _free(mem);
        check_heap(heap, "Heap after free mem");

    end_test(heap, 4096, "test1");
}

//Освобождение одного блока из нескольких выделенных.
void test2(void) {
    void* heap = heap_init(4096);
        if (!heap) error("test2 failed", ", heap is not initialized." );
        check_heap(heap, "Heap after init");

    void *mem1 = _malloc(1024);
    void *mem2 = _malloc(1024);
        if (!mem1 || !mem2) error("test2 failed", ", memory is not allocated.");
        check_heap(heap, "Heap after alloc");

    _free(mem1);
        if ( !mem2 ) error("test2 failed", ", release of the first damaged the second.");
        check_heap(heap, "Heap after free mem");

    _free(mem2);
    end_test(heap, 4096, "test2");
}

//Освобождение двух блоков из нескольких выделенных.
void test3(void) {
    void* heap = heap_init(4096);
        if (!heap) error("test3 failed", ", heap is not initialized." );
        check_heap(heap, "Heap after init");

    void *mem1 = _malloc(1024);
    void *mem2 = _malloc(1024);
    void *mem3 = _malloc(1024);
        if (!mem1 || !mem2 || !mem3) error("test3 failed", ", memory is not allocated.");
        check_heap(heap, "Heap after alloc");

    _free(mem1);
    _free(mem3);
        if ( !mem2 ) error("test3 failed", ", release of the first damaged the second.");
        check_heap(heap, "Heap after free mem");

//    _free(mem2);
    end_test(heap, 4096, "test3");
}

//Память закончилась, новый регион памяти расширяет старый.
void test4(void) {
    void *heap = heap_init(1);
    if (!heap) error("test4 failed", ", heap is not initialized." );
    check_heap(heap, "Heap after init");

    void *mem1 = _malloc(8192);
    void *mem2 = _malloc(16384);
        if (!mem1 || !mem2) error("test4 failed", ", memory is not allocated.");
        check_heap(heap, "Heap after alloc");

    struct block_header *header1 = block_get_header(mem1);
    struct block_header *header2 = block_get_header(mem2);
        if (!header1 || header1->next != header2) error("test4 failed", ", headers are not linked.");

    end_test(heap,(int) (header1->capacity.bytes + header2->capacity.bytes + ( (struct block_header*) heap)->capacity.bytes ), "test4");
//    ?

}

//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
void test5(void) {
    void *heap = heap_init(1);
    if (!heap) error("test5 failed", ", heap is not initialized." );
    check_heap(heap, "Heap after init");

    void *mem1 = _malloc(8192);
    struct block_header *header1 = block_get_header(mem1);
        if (!mem1) error("test5 failed", ", the first memory is not allocated.");
        if (!header1) error("test5 failed", ", header is not find.");
        check_heap(heap, "Heap after alloc.");

    void* newRegion = mmap( (*header1).contents + (*header1).capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0 );

    void *mem2 = _malloc(8192);
    struct block_header *header2 = block_get_header(mem2);
    if (!mem2) error("test5 failed", ", the second memory is not allocated");
        check_heap(heap, "Heap after second malloc");

    _free(mem1);
    _free(mem2);
    check_heap(heap, "Heap after realising");

    munmap(newRegion, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);

    end_test(heap, (int) (header1->capacity.bytes + header2->capacity.bytes + ( (struct block_header*) heap)->capacity.bytes) , "test5");
}